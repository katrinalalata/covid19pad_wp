<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package covid
 */

?>

<footer>
	<div class="contain">
		<div class="left">
		<img class='logo' src="<?php echo get_template_directory_uri(); ?>/assets/img/covidpadlogo.png">
			<p>888-317-5531</p>
		</div>
		<div class="right">
			<div class="links">
				<a>How it works</a>
				<a>Features</a>
				<a>Plans & Pricing</a>

				<a class="action">
					<span>Login</span> / <span>Signup</span></a>
					<button class="btn text-btn">SCHEDULE DEMO</button>
			</div>
		</div>
	</div>
</footer>


<?php wp_footer(); ?>

</body>
</html>


