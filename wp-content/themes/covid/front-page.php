<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package covid
 */

get_header();
?>

<!-- <div class="mobile-nav">
	<p class="close">x</p>
		<div class="links">
			<a>How it works</a>
			<a>Features</a>
			<a>Plans &amp; Pricing</a>

			<a class="action"><span>Login</span> /
		  <span>Signup</span></a><a>SCHEDULE a Demo</a>
		</div>
</div> -->
		  

<!-- BANNER -->
<section class="homepage-hero">
        <div class='banner_holder_covidpad'>
		<div class="site_margins">
            <div class='banner_covid'>

                    <div class="covidpadkiosk">
                        <img class='kiosk0' src="<?php echo get_template_directory_uri(); ?>/assets/img/covidpadkiosk1.png">
                    </div>
                    <div class="covidpadabout">
                        <h2>Stay in CDC compliance and help your employees and customers get back to business.</h2>
						
						<div class="promo">
							<p>Starting at</p>
							<div class="price"><h1>$99</h1><div class="period"><p> / month</p></div></div>
							<p>+ $499 set up fee</p>
							<div class="btns">
								<button class="btn">Register</button>
								<button class="btn text_btn">Login</button>
							</div>
						</div>

                    </div>

            </div>


                </div>
			</div>
</div>
        </div>

</section>

<!-- BENEFITS -->
<section class="covidpad_benefits">
 <div class="covidpad_benefits_title">
        <h3>Our SaaS platform enables businesses to get back to business fast with compliance and confidence</h3>
    </div>
	<div class="site_margins">
        <div class="benefits_flex">
            <div class="benefits_column">
                <div class="benefits_item">
                <img class='warranty' src="<?php echo get_template_directory_uri(); ?>/assets/img/warrantydark.png">
                <p>Auto sending with pre-schedule </p>
                </div>

                <div class="benefits_item">
                <img class='warranty' src="<?php echo get_template_directory_uri(); ?>/assets/img/warrantydark.png">
                <p>Alert your HR on non-compliance activity</p>
                </div>
            </div>

            <div class="benefits_column">
                <div class="benefits_item">
                    <img class='warranty' src="<?php echo get_template_directory_uri(); ?>/assets/img/warrantydark.png">
                    <p>Reports on the go</p>
                </div>

                <div class="benefits_item">
                    <img class='warranty' src="<?php echo get_template_directory_uri(); ?>/assets/img/warrantydark.png">
                    <p>Ease of use and brand customization</p>
                </div>
            </div>

    
        </div>
    </div>
</section>

<!-- FEATURES TITLE -->
<section class="features_about">
    <div class="site_margins">
        <div class="covidpad_flex">
            <h2>COVID19-Pad Complete System</h2>
            <p>Employers are allowed to ask about coronavirus-related symptoms and take the temperatures of employees under guidance from the Equal Employment Opportunity Commission (EEOC), and some states now require it.</p>
        </div>
    </div>
</section>

<!-- Reseller Management System -->
<section class="features_about">
    <div class="site_margins">
        <div class="feature_flex">
            <div class="covid_kiosk">
                <img class='kiosk' src="<?php echo get_template_directory_uri(); ?>/assets/img/cpk1.png">
            </div>
            <div class="feature_text">
                <h2>Feel safe with every scan</h2>
                <p>Employee can scan their pass into our 2D QR scanner. Each scan gets reported to keep your business in compliance.</p>
            </div>
            </div>
    </div>
</section>

<section class="covidpad_green">
    <div class="site_margins">
        <div class="feature_flex_color">
            <div class="covid_kiosk">
                <img class='kiosk' src="<?php echo get_template_directory_uri(); ?>/assets/img/cpk2.png">
            </div>
            <div class="feature_text">
                <h2>Easy access questionaire for all.</h2>
                <p>Get the questionnaire before you come to work. Daily or on the go, You can get it from our digital education display. Scan and go!</p>
                
            </div>
            </div>
    </div>
</section>

<section class="features_about">
    <div class="site_margins">
        <div class="feature_flex">
            <div class="covid_kiosk">
                <img class='kiosk' src="<?php echo get_template_directory_uri(); ?>/assets/img/cpk3.png">
            </div>
            <div class="feature_text">
                <h2>Healthy reminders, everyday.</h2>
                <p>Educate your customers and employees with CDC announcements on social distancing and COVID19 related content. Keep your business safe!</p>

            </div>
            </div>
    </div>
</section>

<!-- CONTACT -->
<section class="covid_contact">
    <div class="site_margins">
        <div class="contact_flex">
            <h1>Contact us for additional information or go to 
            <p>www.covid19pad.com</p></h1>
        </div>
            
            </div>
    </div>
</section>

<?php
// get_sidebar();
get_footer();
