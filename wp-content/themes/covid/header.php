<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package covid
 */

?>

<!--    CUSTOM STYLES-->
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/assets/fonts/fonts.css">

<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<header>
	<div class="contain">
	<img class='logo' src="<?php echo get_template_directory_uri(); ?>/assets/img/covidpadlogo.png">
		<div class="links">
			<a>How it works</a>
			<a>Features</a>
			<a>Plans & Pricing</a>
				
			<a class="action">
				<span>Login</span> / <span>Signup</span></a>
				<button class="btn text-btn">SCHEDULE DEMO</button>
			</div>
			<div class="profile-icon">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/img/hamburger.png"></div>
		</div>
</header>


<?php wp_body_open(); ?>


<!-- <div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#primary"><?php esc_html_e( 'Skip to content', 'covid' ); ?></a>

	<header id="masthead" class="site-header">
		<div class="site-branding">
			<?php
			the_custom_logo();
			if ( is_front_page() && is_home() ) :
				?>
				<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
				<?php
			else :
				?>
				<p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
				<?php
			endif;
			$covid_description = get_bloginfo( 'description', 'display' );
			if ( $covid_description || is_customize_preview() ) :
				?>
				<p class="site-description"><?php echo $covid_description; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></p>
			<?php endif; ?>
		</div> -->
		<!-- .site-branding -->

		<!-- <nav id="site-navigation" class="main-navigation">
			<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><?php esc_html_e( 'Primary Menu', 'covid' ); ?></button>
			<?php
			wp_nav_menu(
				array(
					'theme_location' => 'menu-1',
					'menu_id'        => 'primary-menu',
				)
			);
			?>
		</nav> -->
		<!-- #site-navigation -->
	<!-- </header> -->
	<!-- #masthead -->
